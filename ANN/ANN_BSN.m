clc;clear all;close all;
dataset= [ 10 6 1; % x1(Mass) x2(lenght) target
           20 5 1 ;
           5 4  -1;
           2 5  -1 ;
           3 6 1 ;
           2 5 -1;
           10 7 1 ;
           15 8 1 ; % target = 1 => lorry and if 0 => Van
           5 9  1];  
%dataset=[1 1  1; 1 -1 -1;-1 1 -1;-1 -1 -1];
bnew=0;w1new=0;w2new=0;
bold=bnew  ; 
w1old=w1new;
w2old=w2new ;
j=1; alpha=1;
[row ,col]=size(dataset);
epoch = 30; %y=0;
for it=1:epoch
    for i=1:row
        x1= dataset(i,j);
        x2=dataset(i,j+1);
        t=dataset(i,j+2);
        y_IP= bnew+ (w1new *x1)+ (w2new *x2);
        if (y_IP >= 0)
            y = 1;
%         elseif( y_IP ==0)
%                 y=0;
        elseif (y_IP < 0) 
            y = 0;
        end
        %y=1/( 1+ (exp(-y_IP) ) );
       %disp('Y == T ');
      %disp(y ==t);
       if(y ~= t)
           w1old=w1new;
           w2old=w2new;
           bold=bnew;
%            w1new = w1old + (alpha * (t-y)*x1);
%            w2new= w2old + (alpha * (t-y)*x2);
%            bnew = bold+(alpha*(t-y));
           w1new = w1old + (alpha * (t)*x1);
           w2new= w2old + (alpha * (t)*x2);
           bnew = bold+(alpha*(t));
       end
    end
%     if(w1old==w1new && w2old==w2new && bold==bnew && i==row && it >=1 )
%         break;
%     end
    %disp('iteration');
    %disp(it);
    %epoch = epoch -1;
end

%% displaying results 
j=1;
w1=w1new;
w2=w2new;
b=bnew;
for i=1:row 
        x1= dataset(i,j);
        x2=dataset(i,j+1);
        t=dataset(i,j+2);
        y_IP= b+ (w1 *x1)+ (w2 *x2);
        if (y_IP >= 0)
            y = 1;
%         elseif( y_IP ==0)
%                 y=0;
        elseif (y_IP < 0) 
            y = 0;
        end
        %y=1/( 1+ (exp(-y_IP) ) );
       x1
       x2
       disp('predicted ');
       disp( y);
       disp('target ')
       disp(t);
       disp('***************')
end